<?php

/**
 * @file
 * Contains Drupal\expressions\ExpressionInterface.
 */

namespace Drupal\expressions;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Expression entity.
 */
interface ExpressionInterface extends ConfigEntityInterface {


}
